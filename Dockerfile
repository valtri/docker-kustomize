FROM k8s.gcr.io/kustomize/kustomize:v3.8.7
LABEL maintainer='František Dvořák <valtri@civ.zcu.cz>'

RUN apk add --no-cache git
