# Info

[Kustomize](https://kustomize.io/) docker image with installed git.

# Usage

    docker pull valtri/kustomize
    alias kustomize='docker run -it --rm valtri/kustomie'

    kustomize --help
